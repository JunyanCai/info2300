How to install:

Open the setup.exe to install

How to use:

Character controled by direct keys and SPACE key for jump

Press Enter key to enter different pages

No mouse control allowed

Press ESC to exit present page

The reasons that I choose to use Mozilla Public License:
1. This is the most newest license
2. The version 2.0 has a better license compatibility with other license
3. It is both a free software license recognized by the Free Software Foundation and an open source software license recognized by the Open Source Promotion Association.
4. the program or software licensed under MPL is not subject to patent restrictions and is free to use, sell, and free to redistribute.